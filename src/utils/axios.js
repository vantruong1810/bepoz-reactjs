import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  responseType: 'json',
  maxBodyLength: Infinity,
  maxContentLength: Infinity,
  headers: {
    Accept: 'application/json'
  }
});

export default axiosInstance;
