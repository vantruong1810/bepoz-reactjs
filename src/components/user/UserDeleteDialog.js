import PropTypes from 'prop-types';
import { Button, Dialog, DialogActions, DialogTitle } from '@mui/material';

// ----------------------------------------------------------------------

UserDeleteDialog.propTypes = {
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  onDelete: PropTypes.func
};

export default function UserDeleteDialog({ name, isOpen, onClose, onDelete }) {
  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{`Delete ${name}?`}</DialogTitle>
      <DialogActions>
        <Button onClick={onClose}>Disagree</Button>
        <Button onClick={onDelete} autoFocus>
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
}
