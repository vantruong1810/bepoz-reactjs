import { Link as RouterLink } from 'react-router-dom';
// @mui
import { styled } from '@mui/material/styles';
import { Container, Card, Typography, CardHeader, Stack, Button } from '@mui/material';
// components
import { Icon } from '@iconify/react';
import editFill from '@iconify/icons-eva/edit-fill';
import lockFill from '@iconify/icons-eva/lock-fill';
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import Iconify from '../../components/Iconify';
// hooks
import useAuth from '../../hooks/useAuth';

// ----------------------------------------------------------------------
const IconStyle = styled(Iconify)(({ theme }) => ({
  width: 20,
  height: 20,
  marginTop: 1,
  flexShrink: 0,
  marginRight: theme.spacing(2)
}));

export default function UserProfile() {
  const { user: currentUser } = useAuth();

  return (
    <Page title={`My Profile | ${process.env.REACT_APP_TITLE}`}>
      <Container maxWidth="lg">
        <Stack direction="row" alignItems="center" justifyContent="space-between">
          <HeaderBreadcrumbs
            heading="My Profile"
            links={[{ name: 'Dashboard', href: '/dashboard' }, { name: currentUser.displayName }]}
          />
          <Stack direction="row" alignItems="center" justifyContent="space-between">
            <Button
              color="secondary"
              variant="contained"
              component={RouterLink}
              to="/dashboard/profile/change-password"
              startIcon={<Icon icon={lockFill} />}
            >
              Change password
            </Button>
            <Button
              variant="contained"
              sx={{ ml: 3 }}
              component={RouterLink}
              to="/dashboard/profile/edit"
              startIcon={<Icon icon={editFill} />}
            >
              Edit Profile
            </Button>
          </Stack>
        </Stack>
        <Card>
          <CardHeader title="About" />
          <Stack sx={{ p: 3 }}>
            <Stack sx={{ display: 'flex' }}>
              <Typography variant="body2">
                <IconStyle icon="bx:bxs-user" />
                {currentUser.displayName}
              </Typography>
            </Stack>
            <Stack>
              <Typography variant="body2">
                <IconStyle icon="eva:email-fill" />
                {currentUser.email}
              </Typography>
            </Stack>
            <Stack>
              <Typography variant="body2">
                <IconStyle icon="eva:phone-fill" />
                {currentUser?.phone}
              </Typography>
            </Stack>
            <Stack>
              <Typography variant="body2">
                <IconStyle icon="eva:pin-fill" />
                {currentUser?.address}
              </Typography>
            </Stack>
          </Stack>
        </Card>
      </Container>
    </Page>
  );
}
