import { capitalCase } from 'change-case';
import { useParams, useNavigate } from 'react-router-dom';
// @mui
import { Container } from '@mui/material';
// components
import { useCallback, useEffect, useState } from 'react';
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
// sections
import UserNewForm from '../../components/user/UserNewForm';
import axios from '../../utils/axios';

// ----------------------------------------------------------------------
export default function UserCreate() {
  const { id = 0 } = useParams();
  const navigate = useNavigate();
  const [currentUser, setCurrentUser] = useState({});
  const getUser = useCallback(
    async (id) => {
      try {
        if (id) {
          const response = await axios.get(`/users/${id}/profile`);
          setCurrentUser(response.data.user);
        }
      } catch (error) {
        navigate('/404', { replace: true });
        console.log(error);
      }
    },
    [setCurrentUser, navigate]
  );
  useEffect(() => {
    getUser(id);
  }, [getUser, id]);

  return (
    <Page title={`Create a new user | ${process.env.REACT_APP_TITLE}`}>
      <Container maxWidth="lg">
        <HeaderBreadcrumbs
          heading={!id ? 'Create a new user' : 'Edit user'}
          links={[
            { name: 'Dashboard', href: '/dashboard' },
            { name: 'User List', href: '/dashboard/user' },
            { name: !id ? 'New user' : capitalCase('Truong') }
          ]}
        />

        <UserNewForm currentUser={currentUser} />
      </Container>
    </Page>
  );
}
