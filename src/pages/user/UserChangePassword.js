// @mui
import { Container } from '@mui/material';
// components
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
// hooks
import useAuth from '../../hooks/useAuth';
import UserChangePasswordForm from '../../components/user/UserChangePasswordForm';

// ----------------------------------------------------------------------

export default function UserChangePassword() {
  const { user: currentUser } = useAuth();

  return (
    <Page title={`Change password | ${process.env.REACT_APP_TITLE}`}>
      <Container maxWidth="lg">
        <HeaderBreadcrumbs
          heading="Change password"
          links={[
            { name: 'Dashboard', href: '/dashboard' },
            { name: 'Profile', href: '/dashboard/profile' },
            { name: currentUser.displayName }
          ]}
        />

        <UserChangePasswordForm isEdit currentUser={currentUser} />
      </Container>
    </Page>
  );
}
