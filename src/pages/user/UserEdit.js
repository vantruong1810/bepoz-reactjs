// @mui
import { Container } from '@mui/material';
// components
import Page from '../../components/Page';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
// sections
import UserEditForm from '../../components/user/UserEditForm';
// hooks
import useAuth from '../../hooks/useAuth';

// ----------------------------------------------------------------------

export default function UserEdit() {
  const { user: currentUser } = useAuth();

  return (
    <Page title={`Edit user | ${process.env.REACT_APP_TITLE}`}>
      <Container maxWidth="lg">
        <HeaderBreadcrumbs
          heading="Edit user"
          links={[
            { name: 'Dashboard', href: '/dashboard' },
            { name: 'Profile', href: '/dashboard/profile' },
            { name: currentUser.displayName }
          ]}
        />

        <UserEditForm currentUser={currentUser} />
      </Container>
    </Page>
  );
}
