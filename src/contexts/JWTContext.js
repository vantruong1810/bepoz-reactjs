import { createContext, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
// utils
import axios from '../utils/axios';
import { setSession, isValidToken } from '../utils/jwt';

const initialState = {
  isAuthenticated: false,
  isInitialized: false,
  user: null
};

const userTransformer = (user) => ({
  ...user,
  displayName: `${user?.first_name} ${user?.last_name}`,
  firstName: user?.first_name,
  lastName: user?.last_name,
  photoURL: '/static/mock-images/avatars/avatar_default.jpg'
});

const handlers = {
  INITIALIZE: (state, action) => {
    const { isAuthenticated, user } = action.payload;
    return {
      ...state,
      isAuthenticated,
      isInitialized: true,
      user: userTransformer(user)
    };
  },
  LOGIN: (state, action) => {
    const { user } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user: userTransformer(user)
    };
  },
  LOGOUT: (state) => ({
    ...state,
    isAuthenticated: false,
    user: null
  }),
  REGISTER: (state, action) => {
    const { user } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user: userTransformer(user)
    };
  },
  UPDATE: (state, action) => {
    const { user } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user: userTransformer(user)
    };
  }
};

const reducer = (state, action) =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

const AuthContext = createContext({
  ...initialState,
  method: 'jwt',
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
  register: () => Promise.resolve()
});

AuthProvider.propTypes = {
  children: PropTypes.node
};

function AuthProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const initialize = async () => {
      try {
        const accessToken = window.localStorage.getItem('accessToken');

        if (accessToken && isValidToken(accessToken)) {
          setSession(accessToken);

          const response = await axios.get('/auth/profile');
          const { user } = response.data;

          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: true,
              user: userTransformer(user)
            }
          });
        } else {
          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: false,
              user: null
            }
          });
        }
      } catch (err) {
        console.error(err);
        dispatch({
          type: 'INITIALIZE',
          payload: {
            isAuthenticated: false,
            user: null
          }
        });
      }
    };

    initialize();
  }, []);

  const login = async (email, password) => {
    const response = await axios.post('/auth/login', {
      email,
      password
    });
    const { access_token: accessToken, user } = response.data;

    setSession(accessToken);
    dispatch({
      type: 'LOGIN',
      payload: {
        user: userTransformer(user)
      }
    });
  };

  const register = async (email, password, firstName, lastName) => {
    const response = await axios.post('/auth/register', {
      email,
      password,
      first_name: firstName,
      last_name: lastName
    });
    const { access_token: accessToken, user } = response.data;
    setSession(accessToken);
    dispatch({
      type: 'REGISTER',
      payload: {
        user: userTransformer(user)
      }
    });
  };

  const changePassword = async (oldPassword, password, passwordConfirmation) => {
    const data = {
      old_password: oldPassword,
      password,
      password_confirmation: passwordConfirmation
    };
    await axios.post('/auth/change-password', data);
  };

  const updateProfile = async (userData) => {
    const data = {
      first_name: userData.firstName,
      last_name: userData.lastName,
      ...userData
    };
    const response = await axios.put(`/auth/profile`, data);
    const { user } = response.data;
    dispatch({
      type: 'UPDATE',
      payload: {
        user: userTransformer(user)
      }
    });
  };

  const logout = async () => {
    setSession(null);
    dispatch({ type: 'LOGOUT' });
  };

  return (
    <AuthContext.Provider
      value={{
        ...state,
        method: 'jwt',
        changePassword,
        login,
        logout,
        register,
        updateProfile
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}
export { AuthContext, AuthProvider };
